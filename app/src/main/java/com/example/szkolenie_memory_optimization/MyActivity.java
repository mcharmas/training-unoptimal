package com.example.szkolenie_memory_optimization;

import android.app.ListActivity;
import android.os.Bundle;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class MyActivity extends ListActivity {
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        List<String> images = Arrays.asList("1.jpg", "2.jpg", "3.jpg",
                "4.jpg", "5.png", "6.png");

        List<String> bigList = new LinkedList<String>();
        bigList.addAll(images);
        bigList.addAll(images);
        bigList.addAll(images);
        bigList.addAll(images);
        bigList.addAll(images);

        setListAdapter(new ImageAdapter(this, bigList));

    }
}

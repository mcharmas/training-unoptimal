package com.example.szkolenie_memory_optimization;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

public class ImageAdapter extends ArrayAdapter<String> {

    public ImageAdapter(Context context, List<String> objects) {
        super(context, R.layout.main, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = LayoutInflater.from(getContext()).inflate(R.layout.main, parent, false);
        ImageView imageView = (ImageView) view.findViewById(R.id.icon);
        TextView labelView = (TextView) view.findViewById(R.id.label);
        labelView.setText(getItem(position));

        try {
            InputStream fileStream = getContext().getAssets().open(getItem(position));
            Bitmap bmp = BitmapFactory.decodeStream(fileStream);
            imageView.setImageBitmap(bmp);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return view;
    }
}
